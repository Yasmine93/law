<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!--========Start Section Of Head========-->
    <section class="head">
        <div class="container">
           <?php the_archive_title( '<h2 class="page-title">', '</h2>' ); ?>
        </div>
    </section>
    <!--========End Section Of Head========-->

    <!--========Start Section Of Sidpar========-->
    <section class="services-page section">
        <div class="container">
            <div class="row">
              <div class="col-s-12 col-m-6 col-l-3">
                  <?php get_sidebar(); ?>
              </div>
              <div class="col-s-12 col-m-6 col-l-9">
                <div class="row">

                <?php
		if ( have_posts() ) : ?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				?>
                <div class="col-s-12 col-l-4 ">
                            <div class="all-news">
                                <a href="<?php the_permalink(); ?>" class="block-img" target="_blank" data-src="<?php the_post_thumbnail_url('full'); ?>">
                                </a>
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <a href="<?php the_permalink(); ?>" target="_blank" class="paragraph">
                                <?php the_content(); ?>
                                </a>
                            </div>
                        </div>
<?php endwhile; ?>



                </div>
                  <?php
			the_posts_pagination( array(
				'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
			) );

		else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif; ?>
              </div>
            </div>
        </div>
    </section>
    <!--========End Section Of Sidpar========-->







<?php get_footer();
