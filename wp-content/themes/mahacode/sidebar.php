<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="categoryy">
                        <h3>أقسام الموقع</h3>
	<?php 
	 $menuname = get_post_meta($post->ID, 'menu_name', true);
	if ($menuname != ""){
	wp_nav_menu( array( 'menu' => $menuname, 'container' => '' ) );	
	}else {
	wp_nav_menu( array( 'theme_location' => 'sidebar', 'container' => '' ) ); 
	}
	?>
                        

</div>

                                                        <div class="sidpar-content">
                                                            <h4>دراسات فقهية وأدبية</h4>
                                                             <?php

            $loop = new WP_Query(array('posts_per_page' => '4', 'post_type' => 'drasat', 'orderby' => 'post_id')); ?>
            <?php
                  $i=0;
            while ($loop->have_posts()) : $loop->the_post();
            /*setup_postdata($post2);*/

            ?>

                <a href="<?php the_permalink(); ?>" target="_blank">
               <span><img src="<?php the_post_thumbnail_url('full'); ?>"></span>
              <h5 style="width: 100%;"><?php the_title(); ?></h5>
             </a>
             <?php endwhile;
            wp_reset_postdata(); ?>



            <?php echo paginate_links(); ?>


    </div>
   <?php
   global $wpdb;
   $query = $wpdb->get_results("select * from wp_egvads where place='pages' or place='all' order by arrange asc", ARRAY_A);
   foreach($query as $row){
       ?>
            <a href="<?php echo $row['link']; ?>" target="_blank" class="media" data-src="<?php echo home_url(); ?>/<?php echo $row['pic']; ?>"></a>

       <?php
   }
    ?>
<?php dynamic_sidebar( 'sidebar-1' ); ?>

