<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!--========Start Section Of Header========-->
    <div class="header-parent header-slider tornado-ui " id="home">

     <?php
   global $wpdb;
   $query = $wpdb->get_results("select * from wp_egvsliders order by arrange asc", ARRAY_A);
   foreach($query as $row){
       ?>
        <div class="header" data-src="<?php echo $row['pic']; ?>">
            <div class="container">
                <div class="row row-zCenter">
                    <div class="col-s-12 col-m-8 col-l-8">
                        <div class="header-content">
                            <h1><?php echo $row['title']; ?></h1>
                            <p>
<?php echo $row['description']; ?>
                            </p>
                            <a href="http://preview.mahacode.com/law/%d8%aa%d9%88%d8%a7%d8%b5%d9%84-%d9%85%d8%b9%d9%86%d8%a7/" target="_blank">تواصل معنا </a>
                            <a href="<?php echo $row['link']; ?>" target="_blank">اقرء المزيد</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <?php
   }
    ?>


    </div>
    <!--========End Section Of Header========-->

    <!--========Start Section Of Services========-->
    <section class="section services">
        <div class="container">
            <div class="title">
                <h2> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hammer.png" alt="icon">الخدمات المتاحة</h2>
            </div>
            <div class="row">
             <?php
            $loop = new WP_Query(array('posts_per_page' => '20', 'post_type' => 'services', 'orderby' => 'post_id')); ?>
            <?php

            while ($loop->have_posts()) : $loop->the_post();
            /*setup_postdata($post2);*/
            ?>

                <div class="col-s-12 col-m-6 col-l-4">
                    <div class="services-block">
                       <i> <img src="<?php echo get_post_meta($post->ID, 'icon-link', true); ?>" alt="icon"> </i>
                       
                       

                      


                        <a href="<?php the_permalink() ?>">
                            <h3><?php echo the_title()?></h3>
                        </a>
                    </div>
                </div>

                 <?php endwhile;
            wp_reset_postdata(); ?>



            <?php //echo paginate_links(); ?>

            </div>
        </div>
    </section>
    <!--========End Section Of Services========-->

    <!-----Start Section Of News--------->
    <section class=" section news">
        <div class="container">
            <!---Start Row Of All Content News--->
            <div class="row">

                <div class="col-s-12 col-m-12 col-l-9 ">
                    <!--Start Big Raw For News-->

                  <?php
$categories = get_categories( array(
    'hide_empty'   => 0,
    'taxonomy' => 'categories',
    'orderby' => 'name',
    'order'   => 'ASC'
) );

foreach( $categories as $category ) {
    $category_link = sprintf(
        '<a href="%1$s" alt="%2$s">%3$s</a>',
        esc_url( get_category_link( $category->term_id ) ),
        esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
        esc_html( $category->name )
    );
    $link = get_term_link($category);
    ?>
       <!--Raw Two-->
                    <div class="all-title">
                        <h2 class="title-bg"> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hammer.png"><?php echo $category->name; ?></h2>
                        <a href="<?php echo $link; ?>" target="_blank">قراءة المزيد</a>
                    </div>

                    <div class="row cols-gutter-20 news">
                         <?php
                         $tax = array(  $category->term_id );

            $loop = new WP_Query(array('posts_per_page' => '5', 'post_type' => 'consult',  'tax_query' => array(
        array(
            'taxonomy'  => 'categories',
            'field'     => 'term_id',
            'terms'     => $tax,
        )
    ), 'orderby' => 'post_id')); ?>
            <?php
                  $i=0;
            while ($loop->have_posts()) : $loop->the_post();
            $i++;
            /*setup_postdata($post2);*/
            if ($i == 1){
                       ?>
                             <div class="col-s-12 col-m-6  col-l-4 news-block">
                            <div class=" news-content">
                                <a href="<?php the_permalink() ?>" class="img-block" data-src="<?php the_post_thumbnail_url('full'); ?>"> </a>
                                <a href="<?php the_permalink() ?>" target="_blank" class="address"><?php the_title() ?></a >
                            </div>
                        </div>
                       <?php
            } else {
                 ?>
                      <div class="col-s-12 col-m-6  col-l-2 news-block">
                            <div class=" news-content">
                                <a href="<?php the_permalink() ?>" class="img-block" data-src="<?php the_post_thumbnail_url('full'); ?>"> </a>
                                <a href="<?php the_permalink() ?>" target="_blank" class="address"><?php the_title() ?></a >
                            </div>
                        </div>
                 <?php
            }
            ?>


             <?php endwhile;
            wp_reset_postdata(); ?>



            <?php //echo paginate_links(); ?>

                    </div>
    <?php
} ?>


                </div><!--End Big Raw For News-->

        <!--Start Sidpar--->
        <div class="col-s-12 col-m-6 col-l-3 sidpar hidden-m-down">
        <?php
   global $wpdb;
   $query = $wpdb->get_results("select * from wp_egvads where place='home' or place='all' order by arrange asc", ARRAY_A);
   foreach($query as $row){
       ?>
            <a href="<?php echo $row['link']; ?>" target="_blank" data-src="<?php echo $row['pic']; ?>"></a>

       <?php
   }
    ?>

                                                        <div class="sidpar-content">
                                                            <h4>دراسات فقهية وأدبية</h4>
                                                             <?php

            $loop = new WP_Query(array('posts_per_page' => '4', 'post_type' => 'drasat', 'orderby' => 'post_id')); ?>
            <?php
                  $i=0;
            while ($loop->have_posts()) : $loop->the_post();
            /*setup_postdata($post2);*/

            ?>

                <a href="<?php the_permalink(); ?>" target="_blank">
               <span><img src="<?php the_post_thumbnail_url('full'); ?>"></span>
              <h5 style="width: 100%;"><?php the_title(); ?></h5>
             </a>
             <?php endwhile;
            wp_reset_postdata(); ?>



            <?php //echo paginate_links(); ?>


                                                        </div>
                                                    </div>
                                                    <!--End Sidpar--->

                                                </div>
                                                <!--End Row Of All Content News-->
                                            </div>
    </section>
    <!--========End Section Of News========-->

    <!--========Start Section Of  News Slider========-->
    <section class="news-slider section  Align-center">
        <div class="container">
            <div class="title">
                <h2> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hammer.png" alt="icon">الأخبار القانونية </h2>
                <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى</p>
            </div>
            <div class=" n-slider" >
            <?php

            $loop = new WP_Query(array('posts_per_page' => '10', 'post_type' => 'post', 'orderby' => 'post_id')); ?>

            <?php  $total = $loop->found_posts;
                  $i=0;
                  $slide = 0;
                  $sec = 0;
            while ($loop->have_posts()) : $loop->the_post();
            $i++;
            if ($i == 1){
              $slide=1;
            }
            if ($slide==1){
               echo '<div><div class="row no-gutter">';
            }
            if ($i == 1){
              ?>
              <div class="col-s-12 col-m-6 ">
                            <!--========Right  Block========-->
                            <div class="block1" data-src="<?php the_post_thumbnail_url('full'); ?>">
                                <a href="<?php the_permalink(); ?>" target="_blank">
                                    <h5><?php the_title(); ?></h5>
                                    <p>
                                        <?php echo excerpt(22); ?>
                                    </p>
                                </a>
                            </div>
                        </div>
              <?php
            }
            if ($i == 2){
              $sec = 1;
            }
            if ($sec==1){
               echo ' <div class="col-s-12 col-m-6">
                            <!--========Left 4 Blocks========-->
                            <div class="row no-gutter">';
            }
            if ($i>=2){
                ?>
                 <div class="col-s-12 col-m-6 ">
                                    <div class="block" data-src="<?php the_post_thumbnail_url('full'); ?>">

                                        <a href="<?php the_permalink(); ?>" target="_blank">
                                            <h5><?php the_title(); ?></h5>
                                            <p>
                                                 <?php echo excerpt(10); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                <?php
            }
            if ($sec==4 or $i == $total){
               echo '</div></div>';
            }
            if ($slide==5 or $i == $total){
               echo '</div></div>';
            }
            $slide++;
            $sec++;
            if ($i==5){
              $i=0;
              $slide=0;
              $sec=0;
            }
            /*setup_postdata($post2);*/

            ?>

               <!-- <a href="<?php the_permalink(); ?>" target="_blank">
               <span><img src="<?php the_post_thumbnail_url('full'); ?>"></span>
              <h5 style="width: 100%;"><?php the_title(); ?></h5>
             </a>                   -->


             <?php endwhile;
            wp_reset_postdata(); ?>



            <?php //echo paginate_links(); ?>



            </div>
        </div>
        </div>
    </section>
    <!--========Start Section Of  News Slider========-->

    <!--========Start Section Of  Events========-->
    <section class="events section">
        <div class="container">
            <div class="title">
                <h2> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hammer.png" alt="icon">ندوات ومؤتمرات </h2>
                <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى</p>
            </div>
            <div class="row">

            <?php

            $loop = new WP_Query(array('posts_per_page' => '3', 'post_type' => 'nadawat', 'orderby' => 'post_id')); ?>
            <?php
                  $i=0;
            while ($loop->have_posts()) : $loop->the_post();
            /*setup_postdata($post2);*/

            ?>

                <div class="col-s-12 col-m-6 col-l-4">
                    <div class="event-block">
                        <a href="<?php the_permalink(); ?>" target="_blank" class="img-block" data-src="<?php the_post_thumbnail_url('full'); ?>">
                        </a>
                        <h4><?php the_title(); ?></h4>
                        <a href="<?php the_permalink(); ?>" target="_blank" class="discription">
                       <?php the_excerpt(); ?>
                        </a>
                        <ul class="social" style="display: none;">
                            <li>
                                <a href="#" target="_blank" class="ti-facebook"></a>
                            </li>
                            <li>
                                <a href="#" target="_blank" class="ti-twitter"></a>
                            </li>
                            <li>
                                <a href="#" target="_blank" class="ti-linkedin"></a>
                            </li>
                            <li>
                                <a href="#" target="_blank" class="ti-googleplus"></a>
                            </li>
                        </ul>
                        <a href="<?php the_permalink(); ?>" target="_blank" class="button">المزيد</a>
                    </div>
                </div>

             <?php endwhile;
            wp_reset_postdata(); ?>



            <?php //echo paginate_links(); ?>





            </div>
        </div>
    </section>
    <!--========End Section Of Events========-->

<?php get_footer();
