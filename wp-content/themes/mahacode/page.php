<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

 <!--========Start Section Of Head========-->
    <section class="head">
        <div class="container">
           <?php the_title( '<h2>', '</h2>' ); ?>
        </div>
    </section>
    <!--========End Section Of Head========-->
    <!--========Start Section Of Sidpar========-->
    <section class="services-page section">
        <div class="container">
            <div class="row">
              <div class="col-s-12 col-m-6 col-l-3">
                  <?php get_sidebar(); ?>
              </div>
              <div class="col-s-12 col-m-6 col-l-9">
                  <?php
			while ( have_posts() ) : the_post();
			?>
			<div class="img-block"><img src="<?php the_post_thumbnail_url('full'); ?>"></div>
			<?php
				the_content();

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
              </div>
            </div>
        </div>
    </section>
    <!--========End Section Of Sidpar========-->

<?php  get_footer();
