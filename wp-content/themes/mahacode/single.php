<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

 <!--========Start Section Of Head========-->
    <section class="head">
        <div class="container">
           <?php the_title( '<h2>', '</h2>' ); ?>
        </div>
    </section>
    <!--========End Section Of Head========-->
    <!--========Start Section Of Sidpar========-->
    <section class="services-page section">
        <div class="container">
            <div class="row">
              <div class="col-s-12 col-m-6 col-l-3">
                  <?php get_sidebar(); ?>
              </div>
              <div class="col-s-12 col-m-6 col-l-9">
                <div class="single-news">
                  <?php
			while ( have_posts() ) : the_post();
                 ?>


                        <h3><?php the_title(); ?></h3>
                        <span class="ti-clock"> <?php the_date(); ?></span>
                        <a href="#form" class="ti-question-answer"> أضف تعليقا</a>
                        <div class="img-block"><img src="<?php the_post_thumbnail_url('full'); ?>"></div>
                        <ul class="social">
                            <li>
                                <a href="#" target="_blank" class="ti-facebook"></a>
                            </li>
                            <li>
                                <a href="#" target="_blank" class="ti-twitter"></a>
                            </li>
                            <li>
                                <a href="#" target="_blank" class="ti-linkedin"></a>
                            </li>
                            <li>
                                <a href="#" target="_blank" class="ti-googleplus"></a>
                            </li>
                        </ul>
                       <?php the_content(); ?>







                 <?php          

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
                    ?>

                    <?php
				endif;

			endwhile; // End of the loop.
			?>
             </div>
              </div>
            </div>
        </div>
    </section>
    <!--========End Section Of Sidpar========-->

<?php  get_footer();
