<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php
if ( is_active_sidebar( 'sidebar-2' ) ||
     is_active_sidebar( 'sidebar-3' ) ||
     is_active_sidebar( 'sidebar-4' ) ||
	 is_active_sidebar( 'sidebar-5' ) ) :
?>
      <?php
		if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
			<div class="col-s-12 col-m-6 col-l-4">
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div>
		<?php }
        if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
			<div class="col-s-12 col-m-6 col-l-3">
				<?php dynamic_sidebar( 'sidebar-3' ); ?>
			</div>
		<?php }
        if ( is_active_sidebar( 'sidebar-4' ) ) { ?>
			<div class="col-s-12 col-m-6 col-l-2">
				<?php dynamic_sidebar( 'sidebar-4' ); ?>
			</div>
		<?php }
		if ( is_active_sidebar( 'sidebar-5' ) ) { ?>
			<div class="col-s-12 col-m-6 col-l-3">
				<?php dynamic_sidebar( 'sidebar-5' ); ?>
			</div>
		<?php } ?>

<?php endif; ?>
