<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<!-- Required meta tags always come first -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="A Description Of The Project">
    <meta name="keywords" content="The keywords">
    <title> المفكرة التشريعية </title>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-title.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <!-- Required CSS Files -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/tornado-rtl.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/animations.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/theme.css" rel="stylesheet">

<?php //wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

<!--========Start Section Of Status Par========-->
    <section class="status-par">
        <div class="container">

            <ul class="contacts-links right">
                <li>اتصل بنا : 1254 2254 011</li>
                <li>تواصل معنا : info@Example.com</li>
            </ul>

            <ul class="social left">
                <li>
                    <a href="#" target="_blank" class="ti-facebook"></a>
                </li>
                <li>
                    <a href="#" target="_blank" class="ti-twitter"></a>
                </li>
                <li>
                    <a href="#" target="_blank" class="ti-linkedin"></a>
                </li>
                <li>
                    <a href="#" target="_blank" class="ti-instagram-line"></a>
                </li>
            </ul>
        </div>
    </section>
    <!--========End Section Of Status Par========-->
    <!--========Start Section Of Navbar========-->
<section class="navbar ">
        <div class="container ">

            <nav class="">
                <a class="logo" href="<?php echo home_url( '/' ); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="مستشارك القانونى">
                    <h5> المفكرة التشريعية </h5>
                </a>
                <div class=" right">
                     <div class="navigation-menu">
						 <?php 
					wp_nav_menu(array('menu_class' => '','container' => false,        'container_class' => false,'theme_location' => 'top',));?>
					</div>
                </div>
                <i class="ti-ios-search-strong" data-modal="modal-demo"></i>

                <!-- Modal Box -->
                <div class="modal-box" id="modal-demo">
                    <div class="modal-content">
						<button class="close-modal ti-clear"></button>
                        <h4>كيف يمكننى أن أساعدك</h4>
                        <form action="<?php echo home_url( '/' ); ?>" method="get">
                        <input name="s" placeholder="ابحث هنا">
                        </form>
                    </div>
                </div>
                <a href="#form" class="put">ضع إستشاراتك القانونية</a>
            </nav>


        </div>
    </section>
    <!--========End Section Of Navbar========-->
