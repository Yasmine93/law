<?php
/**
 Template Name: All Posts for Services
 */
 ?>
<?php get_header(); ?>
<!--========Start Section Of Head========-->
    <section class="head">
        <div class="container">
           <h2>ندوات ومؤتمرات</h2>
        </div>
    </section>
<!--========End Section Of Head========-->
<!--========Start Section Of Sidpar========-->
    <section class="services-page section">
        <div class="container">
            <div class="row">
              <div class="col-s-12 col-m-6 col-l-3">
                  <?php get_sidebar(); ?>
              </div>
              <div class="col-s-12 col-m-6 col-l-9">
                <div class="row">
                <?php
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'services', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>

<?php if ( $wpb_all_query->have_posts() ) : ?>



    <!-- the loop -->
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
    <div class="col-s-12 col-l-4 ">
                            <div class="all-news">
                                <a href="<?php the_permalink(); ?>" class="block-img" target="_blank" data-src="<?php the_post_thumbnail_url('full'); ?>">
                                </a>
                                <h3>
                                    <?php the_title(); ?>
                                </h3>
                                <a href="<?php the_permalink(); ?>" target="_blank" class="paragraph">
                                <?php the_excerpt(); ?>
                                </a>
                            </div>
                        </div>
    <?php endwhile; ?>
    <!-- end of the loop -->


    <?php wp_reset_postdata(); ?>

<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>





                </div>
              </div>
            </div>
        </div>
    </section>
    <!--========End Section Of Sidpar========-->


<?php get_footer(); ?>