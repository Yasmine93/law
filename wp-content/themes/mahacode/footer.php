<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<!--========Start Section Of form========-->
    <section class="form-content " id="form">
        <div class="container">
            <div class="row">
                <div class="col-s-12 col-m-12 col-l-7">
                    <div class="row">
                        <div class="form-style" style="width: 100%">
                        <div class="title">
                        <h3> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hammer.png" alt="icon">ضع استشاراتك القانونيه </h3>
                        <h4> سوف يقوم بالرد عليك خبرائنا القانونين</h4>
                        </div>

                        <?php echo do_shortcode( '[contact-form-7 id="123" title="ضع استشارتك القانونيه"]' ); ?>
                        </div>

                    </div>
                </div>
                <div class="col-s-12 col-m-12 col-l-5 hidden-m-down">
					<?php if ( is_active_sidebar( 'contact-1' ) ) { ?>
				<?php dynamic_sidebar( 'contact-1' ); ?>
			
		<?php } ?>
                    
                </div>
            </div>
        </div>
    </section>
    <!--=====Start Section Of form========-->
<!--=======Start Section Of Footer=====-->
    <section class="footer ">
        <div class="container">
            <div class="row">
               <?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
            </div>
        </div>
    </section>
    <!--===== End  Section Of Footer =====-->
<!--=======Start Section Of CopyRight=====-->
    <section class="copy-right">
        <div class="container">
            <div class="row">
                <div class="col-s-12 col-m-6">
                    <p> جميع الحقوق محفوظة © 2017. <span>موقع  المفكرة التشريعية</span> </p>
                </div>
                <div class="col-s-12 col-m-6">
                    <div class="mahacode-copyrights">
                    <a href="http://mahacode.com/" target="_blank" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/Mahacodelogo.png" alt=""></a>
                    <div class="mc-tooltip">
                        <h3>تصميم وتطوير شركة مها كود</h3>
                        <h4 class="ti-email">info@mahacode.com</h4>
                        <h4 class="ti-phone">+02686 4621312 14849 8789</h4>
                        <div class="btns-icons">
                            <a href="http://mahacode.com/" target="_blank" class="ti-home-io"></a>
                            <a href="#" target="_blank"  class="ti-whatsapp-line"></a>
                            <a href="https://www.behance.net/mahacode" target="_blank"  class="ti-behance"></a>
                            <a href="https://www.instagram.com/maha.code/" target="_blank"  class="ti-instagram"></a>
                            <a href="http://www.twitter.com/mahacode" target="_blank"  class="ti-twitter"></a>
                            <a href="https://www.facebook.com/MahaCode/" target="_blank"  class="ti-facebook"></a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!--======= End Section Of CopyRight ======-->
    <!--Start Loading Page ---->
   <div class='tornado-loader  loading2'>
       <div class='spinner'>
           <div class='double-bounce1'>
           </div>
           <div class='double-bounce2'>
           </div>
       </div>
    </div>
    <!--End Loading Page ---->



    <!-- Required JS Files -->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/tornado.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
