<?php
/**
 * إعدادات الووردبريس الأساسية
 *
 * عملية إنشاء الملف wp-config.php تستخدم هذا الملف أثناء التنصيب. لا يجب عليك
 * استخدام الموقع، يمكنك نسخ هذا الملف إلى "wp-config.php" وبعدها ملئ القيم المطلوبة.
 *
 * هذا الملف يحتوي على هذه الإعدادات:
 *
 * * إعدادات قاعدة البيانات
 * * مفاتيح الأمان
 * * بادئة جداول قاعدة البيانات
 * * المسار المطلق لمجلد الووردبريس
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** إعدادات قاعدة البيانات - يمكنك الحصول على هذه المعلومات من مستضيفك ** //

/** اسم قاعدة البيانات لووردبريس */
define('DB_NAME', 'previewm_law');

/** اسم مستخدم قاعدة البيانات */
define('DB_USER', 'previewm_msherif');

/** كلمة مرور قاعدة البيانات */
define('DB_PASSWORD', '654321');

/** عنوان خادم قاعدة البيانات */
define('DB_HOST', 'localhost');

/** ترميز قاعدة البيانات */
define('DB_CHARSET', 'utf8mb4');

/** نوع تجميع قاعدة البيانات. لا تغير هذا إن كنت غير متأكد */
define('DB_COLLATE', '');

/**#@+
 * مفاتيح الأمان.
 *
 * استخدم الرابط التالي لتوليد المفاتيح {@link https://api.wordpress.org/secret-key/1.1/salt/}
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[Xa>W}n9-2-T/2HnKmz0fF0fZ*Ulz?m8*N6=XvC4BM}&M,yRL0UAKCz=4ghOX8)P');
define('SECURE_AUTH_KEY',  'p)8z<=1g5mi6||=>lHJXb{p= ,_9`UnH*Wak}JsBOWyg43qOS!rs8IsRho?-4t^P');
define('LOGGED_IN_KEY',    'Pr^.-9MpG~87:Y,d2L!D<Ax;2D_+Ch,pJ6>g{T(2LK_E4ES1_dpct?HG99f4LNXN');
define('NONCE_KEY',        'o5JbX;5I>mHBN|*ZnLf~74P(didKD?r`gqo$Bv^qo{7G5?)Gjf85x$XFo`P~I$6z');
define('AUTH_SALT',        'CP#8cIx7n`dGXq$=y3.GQ;W`G;TcEW_aj{ew})-OJ=**p-<&.x0#J&Qhv~(=c+Q^');
define('SECURE_AUTH_SALT', 'L~@yb/Ps,0S-LK8ABv!oDhvI`~&)AR(.%ab`Idshg]w42tZ6U4[c(Bz!,5TZbh2$');
define('LOGGED_IN_SALT',   'lFmuRt)iD(hQ1p?IRX.yYK{v6XNWM1W-T@B>M*7Q9z=.-%L31v>lf}@Q(<?M,+81');
define('NONCE_SALT',       'bvbA.K}deXJslwMvz0#He~x*zBq90:^~SK*@wlY Py)EY1OBg$cMzH43OC@eACHy');

/**#@-*/

/**
 * بادئة الجداول في قاعدة البيانات.
 *
 * تستطيع تركيب أكثر من موقع على نفس قاعدة البيانات إذا أعطيت لكل موقع بادئة جداول مختلفة
 * يرجى استخدام حروف، أرقام وخطوط سفلية فقط!
 */
$table_prefix  = 'wp_';

/**
 * للمطورين: نظام تشخيص الأخطاء
 *
 * قم بتغييرالقيمة، إن أردت تمكين عرض الملاحظات والأخطاء أثناء التطوير.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* هذا هو المطلوب، توقف عن التعديل! نتمنى لك التوفيق. */

/** المسار المطلق لمجلد ووردبريس. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** إعداد متغيرات الووردبريس وتضمين الملفات. */
require_once(ABSPATH . 'wp-settings.php');